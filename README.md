# EM Event Detection Demo #

A demonstration of using expectation-maximization to find "spiking" events in calcium imaging data. For now is a notebook that explains the approach. If it is of interest, we could refactor into a module to be used in a pipeline. There are three main sections to the notebook, with a fair bit of redundancy; the third (a mixture of gamma distributions) is the only section needed for data analysis.

**You will need a numpy data file** to run the notebook yourself (except for section 1, which is self-contained). I used a file from Max, F.npy. He posted this file a long time ago, before the NWB standardization, and I have no idea when it was recorded or what was going on in the session (it didn't matter for event detection). All that happens is an array is loaded, and the code pulls out the first row:

```python
A = np.load('F.npy')
df_data = A[0,:]
```

Only `data_df` is used from there on. Probably any such file will work, or you can replace the load block with something else, though with a different recording parameters may need to be tweaked to get good performance.

I included a PDF of the notebook so that you can see what the notebook is doing even without having to find an appropriate data file.

## Setup

To create your environment from scratch:
```
conda env create -f environment.yml
```
Then activate your environment:
```
conda activate em-event-detection-demo
```
Add your new environment (kernel) in Jupyter:
```
python -m ipykernel install --user --name=em-event-detection-demo
```
Finally you can run JupyterLab  with the following command:

```
jupyter lab
```
